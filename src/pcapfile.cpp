#include "../include/pcapfile.h"

// Returns filename without extension .pcap
string PcapFile::getFileNameWithoutExtension()
{
    string name;
    int len = strlen(fileName);
    name.assign(fileName, fileName + (len - 5));

    return name;
}

//Prints count of each IPV4 address
void PcapFile::printIPV4UniqueCount()
{
    for (pair<ipv4address, unsigned int> element : uniqueIPV4Count)
    {
        cout << element.first.getIPAddress() << " : ";
        cout << element.second << endl;
    }
}

//Prints count of each IPV6 address
void PcapFile::printIPV6UniqueCount()
{
    for (pair<ipv6address, unsigned int> element : uniqueIPV6Count)
    {
        cout << element.first.getIPAddress() << " : ";
        cout << element.second << endl;
    }
}

//write IPV4 and IPV6 address's count to a file
void PcapFile::writeIPUniquecount()
{
    string filename = getFileNameWithoutExtension();
    filename = filename + "count.csv";
    string folderpath = getFileFolderPath();
    string outputpath = "output" + folderpath;
    string filepath = outputpath + "/" + filename;

    fstream fout;
    fout.open(filepath, fstream::out | ios::trunc);

    for (pair<ipv4address, unsigned int> element : uniqueIPV4Count)
    {
        fout << element.first.getIPAddress() << ",";
        fout << element.second << "\n";
    }

    for (pair<ipv6address, unsigned int> element : uniqueIPV6Count)
    {
        fout << element.first.getIPAddress() << ",";
        fout << element.second << "\n";
    }
}

//Returns file size in bytes
long PcapFile::getFileSize()
{
    string path = getFilePath();
    struct stat buffer;
    int rc = stat(path.c_str(), &buffer);
    return rc == 0 ? buffer.st_size : -1;
}

//Returns true if file is accessible else false
bool PcapFile::isAccessible()
{
    string path = getFilePath();
    struct stat buffer;
    return (stat(path.c_str(), &buffer) == 0);
}

//Returns file size in bytes
string PcapFile::getFileMtime()
{
    string path = getFilePath();
    struct stat buffer;
    int rc = stat(path.c_str(), &buffer);
    return string(ctime(&buffer.st_mtime));
}