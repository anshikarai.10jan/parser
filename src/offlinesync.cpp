#include "../include/offlinesync.h"
#include <log4cpp/PropertyConfigurator.hh>
#include <log4cpp/Category.hh>
#define LOGCONFIGFILE "log4cpp.properties"
#include <chrono>
using namespace std::chrono;

OfflineSync *OfflineSync::offlinesync = NULL;
OfflineSync *OfflineSync::getInstance()
{
    if (!offlinesync)
        offlinesync = new OfflineSync();

    return offlinesync;
}

void OfflineSync::updateIndex(PcapFile *pcapfile)
{
    string filepath = pcapfile->getFilePath();
    string filemtime = pcapfile->getFileMtime();
    filemtime.pop_back();
    index.insert({filepath, filemtime});
}

void OfflineSync::writeIndex()
{
    fstream fout;
    fout.open("index.csv", fstream::out);

    for (pair<string, string> pair : index)
    {
        fout << pair.first << ",";
        fout << pair.second << "\n";
    }
    fout.close();
}

void OfflineSync::readIndex()
{
    fstream fin;
    fin.open("index.csv", ios::in);

    while (fin)
    {
        string key, value;
        string row;

        getline(fin, row);
        stringstream s_stream(row);
        if (fin.eof())
            break;

        getline(s_stream, key, ',');
        getline(s_stream, value, ',');
        index.insert({key, value});
    }

    fin.close();
}

void OfflineSync::parseFiles(string path)
{
    //Using log4cpp for logging
    std::string initFileName = LOGCONFIGFILE;
    log4cpp::PropertyConfigurator::configure(initFileName);
    log4cpp::Category &logger =
        log4cpp::Category::getInstance(std::string("sub1"));

    log4cpp::Category &warnlogger =
        log4cpp::Category::getInstance(std::string("sub2"));

    DIR *dir;
    struct dirent *entry;

    if (!(dir = opendir(path.c_str())))
    {
        warnlogger.error("%s %d Unable to get DIR stream on directory : ", __FILE__, __LINE__, path);
        return;
    }

    while ((entry = readdir(dir)) != NULL)
    {
        char *ptr1 = NULL, *ptr2 = NULL;
        string temppath = path;
        string name = entry->d_name;
        if (entry->d_type == DT_DIR)
        {
            //strmp returns 0 if both strings are identical
            if (!strcmp(name.c_str(), ".") || !strcmp(name.c_str(), ".."))
                continue;
            temppath.push_back('/');
            temppath = temppath + name;
            parseFiles(temppath);
        }
        else
        {
            int retn = 0;
            ptr1 = strtok(entry->d_name, ".");
            ptr2 = strtok(NULL, ".");
            if (ptr2 != NULL)
            {
                retn = strcmp(ptr2, "pcap");
                if (retn == 0)
                {
                    clock_t start, end;
                    logger.debug("%s %d Creating pcapfile object", __FILE__, __LINE__);
                    // Creating a file object and setting filename and filepath
                    PcapFile *pcapfile = new PcapFile();
                    string filepath = temppath + "/" + name;
                    pcapfile->setFileName(name.c_str());
                    pcapfile->setFileFolderPath(temppath.c_str());
                    pcapfile->setFilePath(filepath.c_str());

                    //check if file not already parsed
                    if (index.find(filepath) == index.end())
                    {
                        logger.debug("%s %d Creating pcapparser object", __FILE__, __LINE__);
                        // Create a PcapParser object and call method parse to parse the file
                        PcapParser *pcapparser = new PcapParser(pcapfile);
                        auto start = high_resolution_clock::now();
                        pcapparser->parse();
                        auto stop = high_resolution_clock::now();
                        auto duration = duration_cast<seconds>(stop - start);
                        cout << duration.count() << endl;
                    }
                    else //if file is already parsed check for modification
                    {
                        string mtimeold, mtimenew;

                        mtimeold = index[filepath];
                        mtimenew = pcapfile->getFileMtime();
                        mtimenew.pop_back();
                        cout << "'" << mtimeold << "'" << endl;
                        cout << "'" << mtimenew << "'" << endl;

                        if (mtimeold.compare(mtimenew) != 0)
                        {
                            logger.debug("%s %d Creating pcapparser object", __FILE__, __LINE__);
                            // Create a PcapParser object and call method parse to parse the file
                            PcapParser *pcapparser = new PcapParser(pcapfile);

                            auto start = high_resolution_clock::now();
                            pcapparser->parse();
                            auto stop = high_resolution_clock::now();
                            auto duration = duration_cast<seconds>(stop - start);
                            cout << duration.count() << endl;
                        }
                    }
                }
            }
        }
    }
    closedir(dir);
}