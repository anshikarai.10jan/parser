#include <log4cpp/PropertyConfigurator.hh>
#include <log4cpp/Category.hh>
#include "../include/pcapparser.h"
#include "../include/offlinesync.h"
#include <netinet/in.h>
#define LOGCONFIGFILE "log4cpp.properties"

//checks if a file is really a pcap file
bool PcapParser::isPcap()
{
	return global_header.magic_number == MAGICNUMBER1 || global_header.magic_number == MAGICNUMBER2 ||
		   global_header.magic_number == MAGICNUMBER3 || global_header.magic_number == MAGICNUMBER4;
}

//parse method takes file name as
void PcapParser::parse()
{
	//Using log4cpp for logging
	std::string initFileName = LOGCONFIGFILE;
	log4cpp::PropertyConfigurator::configure(initFileName);
	log4cpp::Category &logger =
		log4cpp::Category::getInstance(std::string("sub1"));

	log4cpp::Category &warnlogger =
		log4cpp::Category::getInstance(std::string("sub2"));

	//Open file and read first 4 bytes i.e, magic number
	inFile.open(pcapFile->getFilePath(), ios::binary);
	inFile.read((char *)&global_header.magic_number, 4);

	//check if it is a pcap file and parse
	if (isPcap())
	{

		//create and open a file named as filename.csv
		string filename = pcapFile->getFileNameWithoutExtension();
		filename = filename + ".csv";
		string folderpath = pcapFile->getFileFolderPath();
		string outputpath = "output" + folderpath;
		string filepath = outputpath + "/" + filename;
		system(("mkdir -p " + outputpath).c_str());

		fstream fout;
		fout.open(filepath, fstream::out | ios::trunc);

		logger.info("%s %d Started parsing pcap file %s", __FILE__, __LINE__, pcapFile->getFileName());
		cout << "Started parsing pcapfile " << pcapFile->getFileName() << endl;
		//reading global header
		inFile.read((char *)&global_header.version_major, 2);
		inFile.read((char *)&global_header.version_minor, 2);
		inFile.read((char *)&global_header.timezone, 4);
		inFile.read((char *)&global_header.timeaccuracy, 4);
		inFile.read((char *)&global_header.snaplen, 4);
		inFile.read((char *)&global_header.network, 4);

		int packet_count = 0;
		long progress = 24;
		long file_size = pcapFile->getFileSize();
		double progress = 0.0;

		//read file until pointer reached to end of file
		while (inFile.peek() != EOF)
		{
			++packet_count;

			logger.debug("%s %d Read packet header ", __FILE__, __LINE__);
			//reading packet header
			inFile.read((char *)&packet_header.ts_sec, 4);
			inFile.read((char *)&packet_header.ts_Usec, 4);
			inFile.read((char *)&packet_header.incl_len, 4);
			inFile.read((char *)&packet_header.orig_len, 4);

			seek = packet_header.incl_len;

			logger.debug("%s %d Read ethernet header ", __FILE__, __LINE__);
			//read ethernet header and store its field packettype
			inFile.read((char *)&ethernet_header.destination.mac_address, 6);
			inFile.read((char *)&ethernet_header.source.mac_address, 6);
			inFile.read((char *)&ethernet_header.packettype, 2);
			ethernet_header.packettype = ntohs(ethernet_header.packettype);

			seek = seek - 14;
			//check if encapsulated packet is IPV4 or IPV6
			switch (ethernet_header.packettype)
			{
			case IPV4:

				logger.debug("%s %d Read IPV4 header ", __FILE__, __LINE__);
				//read IPV4 packet
				inFile.read((char *)&ipv4_header.version_headerlen, 1);
				inFile.read((char *)&ipv4_header.service_field, 1);
				inFile.read((char *)&ipv4_header.total_length, 2);
				inFile.read((char *)&ipv4_header.identification, 2);
				inFile.read((char *)&ipv4_header.flags_fragementoffset, 2);
				inFile.read((char *)&ipv4_header.ttl, 1);
				inFile.read((char *)&ipv4_header.protocol, 1);
				inFile.read((char *)&ipv4_header.checksum, 2);
				inFile.read((char *)&ipv4_header.source_ip.ip_address, 4);
				inFile.read((char *)&ipv4_header.destination_ip.ip_address, 4);

				seek = seek - 20;
				//increment IPV4 packet count
				pcapFile->incrementIPV4Count();

				//increment IPV4 address's counts
				pcapFile->updateIPV4UniqueCount(ipv4_header.source_ip, ipv4_header.destination_ip);

				//unwrap ip packet to retrive tcp or udp packet
				unwrapIPPacket(ipv4_header.protocol);
				break;

			case IPV6:

				//logger.debug("%s %d Read IPV6 header ", __FILE__, __LINE__);
				//read IPV6 packet
				inFile.read((char *)&ipv6_header.version_priority_flowlable, 4);
				inFile.read((char *)&ipv6_header.payload_length, 2);
				inFile.read((char *)&ipv6_header.protocol, 1);
				inFile.read((char *)&ipv6_header.hop_Limit, 1);
				inFile.read((char *)&ipv6_header.source_ip.ipv6_address, 16);
				inFile.read((char *)&ipv6_header.destination_ip.ipv6_address, 16);

				seek = seek - 40;
				//print IPV6 header
				//pv6_header.printIPPacket();

				//incremenet IPV6 packet count
				pcapFile->incrementIPV6Count();

				//increment IPV4 address's counts
				pcapFile->updateIPV6UniqueCount(ipv6_header.source_ip, ipv6_header.destination_ip);

				//unwrap ip packet to retrive tcp or udp packet
				unwrapIPPacket(ipv6_header.protocol);
				break;

			default:
				break;
			}

			//increment pointer to point to next packet header
			inFile.seekg(seek, ios::cur);

			//write all the required details to a file
			logger.debug("%s %d write packet called", __FILE__, __LINE__);
			writePacket(fout);

			//calculate file parsing progress and show on console
			progress = progress + packet_header.incl_len + 16;
			progress = (progress * 100) / file_size;
			showProgress(progress);
		}

		//print details to console
		cout << endl
			 << "-----------------Details----------------" << endl;
		cout << "IPV4 Count: " << pcapFile->getIPV4Count() << endl;
		cout << "IPV6 Count: " << pcapFile->getIPV6Count() << endl;
		cout << "UDP Count: " << pcapFile->getUDPCount() << endl;
		cout << "TCP Count: " << pcapFile->getTCPCount() << endl;
		cout << endl
			 << "Packet Count " << packet_count << endl;

		//write IPV4 and IPV6 address's count to a files
		pcapFile->writeIPUniquecount();
		cout << endl;
		OfflineSync::getInstance()->updateIndex(pcapFile);

		fout.close();

		logger.info("Parsing completed %s ", pcapFile->getFileName());
	}
	else
	{
		logger.info("%s %d Not a pcap file %s", __FILE__, __LINE__, pcapFile->getFileName());
		cout << "Not a pcapfile " << pcapFile->getFileName() << endl;
	}
	log4cpp::Category::shutdown();
}

//unwrapIPPacket method checks if ip packet contains tcp packet or udp packet
//parse packet and store required fields
void PcapParser::unwrapIPPacket(unsigned char protocol)
{

	switch (protocol)
	{
	case TCP:
		//parse TCP packet
		inFile.read((char *)&tcp_header.source_port, 2);
		inFile.read((char *)&tcp_header.destination_port, 2);
		inFile.read((char *)&tcp_header.sequence_number, 4);
		inFile.read((char *)&tcp_header.ack_number, 4);
		inFile.read((char *)&tcp_header.headerlength_flags, 2);
		inFile.read((char *)&tcp_header.window_size, 2);
		inFile.read((char *)&tcp_header.checksum, 2);
		inFile.read((char *)&tcp_header.urgent_pointer, 2);

		seek = seek - 20;
		//store source and destination port
		tcp_header.source_port = ntohs(tcp_header.source_port);
		tcp_header.destination_port = ntohs(tcp_header.destination_port);

		//print tcp packet details
		//tcp_header.printTCPPacket();

		//increment tcp packet count
		pcapFile->incrementTCPCount();
		break;

	case UDP:
		//parse udp packet
		inFile.read((char *)&udp_header.source_port, 2);
		inFile.read((char *)&udp_header.destination_port, 2);
		inFile.read((char *)&udp_header.length, 2);
		inFile.read((char *)&udp_header.checksum, 2);

		seek = seek - 8;
		//store source and destination port
		udp_header.source_port = ntohs(udp_header.source_port);
		udp_header.destination_port = ntohs(udp_header.destination_port);

		//increment udp packet count
		pcapFile->incrementUDPCount();
		break;

	default:
		break;
	}
}

//writePacket method writes packet details into a file
void PcapParser::writePacket(fstream &fout)
{

	//write ethernet packet details
	fout << ethernet_header.source.getMacAddress() << ", "
		 << ethernet_header.destination.getMacAddress() << ", ";

	//check if ethernet contains IPV4 or IPV6 packet type
	switch (ethernet_header.packettype)
	{
	case IPV4:

		//write IPV4 packet details
		fout << "IPV4"
			 << ","

			 << ipv4_header.source_ip.getIPAddress() << ","
			 << ipv4_header.destination_ip.getIPAddress() << ",";

		//check if IPV4 contains TCP or UDP packet type
		switch (ipv4_header.protocol)
		{
		case TCP:

			//write tcp packet details
			fout << "TCP"
				 << ","
				 << tcp_header.source_port << ", "
				 << tcp_header.destination_port << ", ";
			break;
		case UDP:

			//write udp packet details
			fout << "UDP"
				 << ","
				 << udp_header.source_port << ", "
				 << udp_header.destination_port << ", ";
			break;
		default:
			break;
		}
		fout << "\n";
		break;

	case IPV6:

		//write IPV4 packet details
		fout << "IPV6"
			 << ","
			 << ipv6_header.source_ip.getIPAddress() << ","
			 << ipv6_header.destination_ip.getIPAddress() << ",";

		//check if IPV6 contains TCP or UDP packet type
		switch (ipv6_header.protocol)
		{
		case TCP:

			//write tcp packet details
			fout << "TCP"
				 << ","
				 << tcp_header.source_port << ", "
				 << tcp_header.destination_port << ", "
				 << "\n";
			break;
		case UDP:

			//write udp packet details
			fout << "UDP"
				 << ","
				 << udp_header.source_port << ", "
				 << udp_header.destination_port << ", "
				 << "\n";
			break;
		default:
			fout << "\n";
			break;
		}
		break;
	}
}

//showProgress method takes progress percentage as input
// and draw a progress bar on the console
void PcapParser::showProgress(double progress)
{
	int barWidth = 100;
	cout << "[";
	int pos = (int)progress;
	for (int i = 0; i < barWidth; i++)
	{
		if (i < pos)
			cout << "=";
		else if (i == pos)
			cout << ">";
		else
			cout << " ";
	}
	cout << "] " << progress << " %\r";
	cout.flush();
}