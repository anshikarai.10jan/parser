#include "../include/watcher.h"
#include "../include/pcapparser.h"
#include <log4cpp/PropertyConfigurator.hh>
#include <log4cpp/Category.hh>
#define LOGCONFIGFILE "log4cpp.properties"
#include <chrono>
using namespace std::chrono;

//stores true or false to stop watcher
bool Watcher::stop;
unordered_map<int, string> Watcher::keys;

// Watcher is a singleton class, returns same object everytime
Watcher *Watcher::watcher = 0;
Watcher *Watcher::instance()
{
	if (!watcher)
		watcher = new Watcher();

	return watcher;
}

void Watcher::walkAndRegisterDirectories(int fd, string path)
{
	//Using log4cpp for logging
	std::string initFileName = LOGCONFIGFILE;
	log4cpp::PropertyConfigurator::configure(initFileName);
	log4cpp::Category &logger =
		log4cpp::Category::getInstance(std::string("sub1"));

	log4cpp::Category &warnlogger =
		log4cpp::Category::getInstance(std::string("sub2"));

	DIR *dir;
	struct dirent *entry;

	if (!(dir = opendir(path.c_str())))
	{
		warnlogger.error("%s %d Unable to get DIR stream on directory : ", __FILE__, __LINE__, path);
		return;
	}

	while ((entry = readdir(dir)) != NULL)
	{
		char *ptr1 = NULL, *ptr2 = NULL;
		string temppath = path;
		string name = entry->d_name;
		if (entry->d_type == DT_DIR)
		{
			//strmp returns 0 if both strings are identical
			if (!strcmp(name.c_str(), ".") || !strcmp(name.c_str(), ".."))
				continue;
			temppath.push_back('/');
			temppath = temppath + name;
			registerDirectory(fd, temppath);
			logger.info("%s %d walk and register diretories called over %s", __FILE__, __LINE__, temppath);
			walkAndRegisterDirectories(fd, temppath);
		}

		
	}
	closedir(dir);
}

void Watcher::registerDirectory(int fd, string dir)
{
	//Using log4cpp for logging
	std::string initFileName = LOGCONFIGFILE;
	log4cpp::PropertyConfigurator::configure(initFileName);
	log4cpp::Category &logger =
		log4cpp::Category::getInstance(std::string("sub1"));
	log4cpp::Category &warnlogger =
		log4cpp::Category::getInstance(std::string("sub2"));

	int wd;
	logger.debug("%s %d Register directory %s", __FILE__, __LINE__, dir);
	wd = inotify_add_watch(fd, dir.c_str(), IN_ALL_EVENTS);

	if (wd == -1)
	{
		warnlogger.warn("%s %d Couldn't register directory", __FILE__, __LINE__, dir);
	}
	else
	{
		keys.insert({wd, dir});
	}

	log4cpp::Category::shutdown();
}

// Argument void* arg in method watch points to path of a directory to watch
void *Watcher::watch(void *arg)
{
	//Using log4cpp for logging
	std::string initFileName = LOGCONFIGFILE;
	log4cpp::PropertyConfigurator::configure(initFileName);
	log4cpp::Category &logger =
		log4cpp::Category::getInstance(std::string("sub1"));

	log4cpp::Category &warnlogger =
		log4cpp::Category::getInstance(std::string("sub2"));

	//converting void pointer to char pointer
	const char *path = (char *)arg;

 
	// Initialize Inotify
	// fd stands for file descriptor
	int fd = inotify_init();
	if (fd >= 0)
	{
		// Add watch to directory
		// wd stands for watch desciptor
		registerDirectory(fd, path);
		logger.info("%s %d walk and register diretories called over %s", __FILE__, __LINE__, path);
		walkAndRegisterDirectories(fd, path);

		logger.info("%s %d watching directory %s for any new files", __FILE__, __LINE__, path);
		cout << "watching directory " << path << " for any new files " << endl;

		int length = 0, index = 0;
		char buffer[BUF_LEN];

		// Watch until user enter anything to stop
		while (!(Watcher::stop))
		{

			index = 0;
			length = read(fd, buffer, BUF_LEN);
			if (length < 0)
			{
				warnlogger.error("%s %d Unable to read event from buffer: Returned value %d", __FILE__, __LINE__, length);
			}
			while (index < length)
			{
				struct inotify_event *event = (struct inotify_event *)&buffer[index];
				if (event->len)
				{

					if (event->mask & IN_CLOSE_WRITE || event->mask & IN_MOVED_TO)
					{
						if (!(event->mask & IN_ISDIR))
						{
							//clock_t start,end;
							logger.info("%s %d The file %s is Created %s", __FILE__, __LINE__, event->name);
							cout << "The file " << event->name << " is Created " << endl;
							// Creating a file object and setting filename and filepath
							logger.debug("%s %d Creating pcapfile object", __FILE__, __LINE__);
							PcapFile *pcapfile = new PcapFile();

							string filefolderpath = keys[event->wd];
							string filepath = filefolderpath + "/" + event->name;
							pcapfile->setFileName(event->name);
							pcapfile->setFileFolderPath(filefolderpath.c_str());
							pcapfile->setFilePath(filepath.c_str());

							// Create a PcapParser object and call method parse to parse the file
							logger.debug("%s %d Creating pcapparser object", __FILE__, __LINE__);
							
							PcapParser *pcapparser = new PcapParser(pcapfile);
							auto start = high_resolution_clock::now(); 
                            pcapparser->parse();
                            auto stop = high_resolution_clock::now(); 
                            auto duration = duration_cast<seconds>(stop-start);
                            cout<<duration.count()<<endl;

							
							
						}
					}

					if(event->mask & IN_CREATE)
					{

						if(event->mask & IN_ISDIR)
						{
							logger.info("%s %d The directory %s is Created %s", __FILE__, __LINE__, event->name);
							cout << "The Directory " << event->name << " is Created " << endl;

							string filepath = keys[event->wd];
							string filename =event->name;
							string dir = filepath + "/" + filename;
							registerDirectory(fd, dir);
							logger.info("%s %d walk and register diretories called over %s", __FILE__, __LINE__, path);
							walkAndRegisterDirectories(fd, dir);
						}
					}



				}
				index += EVENT_SIZE + event->len;
			}
		}
	}
	else
	{
		warnlogger.error("%s %d Couldn't initialize inotify", __FILE__, __LINE__);
	}
	//close file descriptor
	close(fd);
	
	log4cpp::Category::shutdown();
}
