#include <iostream>
#include <sys/stat.h>
#include "../include/watcher.h"
#include "../include/offlinesync.h"
#include <log4cpp/PropertyConfigurator.hh>
#include <log4cpp/Category.hh>
#include <log4cpp/LayoutAppender.hh>

using namespace std;
#define VALIDDIRECTORY 0
#define TRUE 1
#define FALSE 0

int main()
{
    OfflineSync::getInstance()->readIndex();
    //Using log4cpp for logging
    std::string initFileName = "log4cpp.properties";
    log4cpp::PropertyConfigurator::configure(initFileName);
    log4cpp::Category &logger =
        log4cpp::Category::getInstance(std::string("sub1"));

    log4cpp::Category &warnlogger =
        log4cpp::Category::getInstance(std::string("sub2"));

    char path[200];

    //Take a valid directory path from user as a input
    while (!VALIDDIRECTORY)
    {
        cout << "Enter directory path :" << endl;
        cin.getline(path, 200);

        struct stat info;
        if (stat(path, &info) != 0)
        {
            cout << "Not a directory" << endl;
            logger.info("%s %d Directory Entered: %s \n", __FILE__, __LINE__, path);
        }
        else if (info.st_mode & S_IFDIR)
        {
            logger.info("Directory Entered: %s \n", path);
            break;
        }
        else
        {
            cout << "Not a directory" << endl;
            logger.info("Directory Entered: %s \n", path);
        }
    }
    logger.info("%s %d parseFiles of class OfflineSync called", __FILE__, __LINE__);
    OfflineSync::getInstance()->parseFiles(path);

    //Initializing and creating thread to watch over a folder
    int threadcreationfailed = 0;
    pthread_t thread;
    logger.debug("%s %d Initalizing watcher class", __FILE__, __LINE__);
    Watcher *watcher = Watcher::instance();

    logger.debug("%s %d Calling thread to watch over directory %s", __FILE__, __LINE__, path);
    threadcreationfailed = pthread_create(&thread, NULL, watcher->watch, (void *)path);
    watcher->setStop(FALSE);

    //If thread creation fails, terminate program
    if (threadcreationfailed)
    {
        warnlogger.fatal("%s %d Unable to create a thread to watch, terminating program", __FILE__, __LINE__);
        exit(-1);
    }

    bool stop = 0;
    cout << "Enter anything to stop" << endl;
    cin >> stop;
    watcher->setStop(TRUE); // to stop watcher and terminate program

    OfflineSync::getInstance()->writeIndex();
    cout << "closing program.." << endl;
    logger.info("closing program..");
    log4cpp::Category::shutdown();

    return 0;
}