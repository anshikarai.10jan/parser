#ifndef PACKETHEADER
#define PACKETHEADER
#include "typedef.h"
#include <iostream>
using namespace std;

/*
* Structure defining packet header of wireshark's generated pcap file.
*/
typedef struct pcap_packet_hdr_struc
{
	u_int4 ts_sec;	 //time stamp seconds
	u_int4 ts_Usec;	 //time stamp microseconds
	u_int4 incl_len; //total captured packet length
	u_int4 orig_len; //total original packet length

} pcap_packet_hdr;

#endif
