#ifndef GLOBALHEADER
#define GLOBALHEADER
#include "typedef.h"

/*
* Structure defining global header of wireshark's generated pcap file.
*/
typedef struct pcap_global_hdr_struc
{

	u_int4 magic_number;  // magic number
	u_int2 version_major; // major version number
	u_int2 version_minor; // minor version number
	u_int4 timezone;	  // GMT to local
	u_int4 timeaccuracy;  // accuracy of timestamps
	u_int4 snaplen;		  // snapshot length
	u_int4 network;		  // link-layer header type

} pcap_global_hdr;

#endif