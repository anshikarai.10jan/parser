#ifndef PCAPPARSER
#define PCAPPARSER
#include "pcapheader.h"
#include "pcapfile.h"
#include <iostream>
using namespace std;

/*
*class PcapParser parses the .pcap file 
*and writes its required contents to .csv file
*/
class PcapParser
{

private:
	PcapFile *pcapFile;
	pcap_global_hdr global_header;
	pcap_packet_hdr packet_header;
	pcap_ethernet_hdr ethernet_header;
	pcap_ipv4_hdr ipv4_header;
	pcap_ipv6_hdr ipv6_header;
	pcap_tcp_hdr tcp_header;
	pcap_udp_hdr udp_header;
	int seek=0;
	ifstream inFile;
	enum IP
	{
		IPV4 = 2048, //0x0800
		IPV6 = 34525 //0x86DD
	};
	enum protocol
	{
		TCP = 6,
		UDP = 17
	};

	enum pcap
	{
		MAGICNUMBER1 = 2712847316, //0xa1b2c3d4
		MAGICNUMBER2 = 3569595041, //0xd4c3b2a1
		MAGICNUMBER3 = 2712812621, //0Xa1b23c4d
		MAGICNUMBER4 = 1295823521  //0X4d3cb2a1
	};

public:
	PcapParser(PcapFile *file)
	{
		pcapFile = file;
	}
	~PcapParser(){ delete pcapFile;
	}
	bool isPcap();		// Returns true if file is a pcap file else false
	void parse();		// Parses the pcap file
	void unwrapIPPacket(unsigned char protocol);	//Extracts IP packet
	void writePacket(fstream& fout);								//Write packets to a file
	void showProgress(double progress);				//Show progress on console
};

#endif