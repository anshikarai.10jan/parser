#ifndef WATCHER
#define WATCHER
#include <sys/inotify.h>
#include <unistd.h>
#include <pthread.h>
#include <iostream>
#include <unordered_map>
#include <time.h>
#include <sys/types.h>
#include <cstring>
#include <dirent.h>
#include<iostream> 
#include<string> 

using namespace std;
using namespace std;

#define MAX_EVENTS 1024								   // Max. number of events to process at one go
#define LEN_NAME 16									   // Assuming that the length of the filename won't exceed 16 bytes
#define EVENT_SIZE (sizeof(struct inotify_event))	   // size of one event
#define BUF_LEN (MAX_EVENTS * (EVENT_SIZE + LEN_NAME)) // buffer to store the data of events
#define FALSE 0
#define TRUE 1

/*
* Watcher class watches over a given directory for creation of any new files
* and calls parse method of class pcapparser to parse them.
*/
class Watcher
{
private:
	static bool stop;		 // stores true or false to stop watcher
	static Watcher *watcher; // watcher object (watcher is a singleton class)
	Watcher() {}			 // private constructor
	~Watcher(){}
  	static unordered_map<int, string> keys; 
public:
	static Watcher *instance(); // Returns object of the class
	static void setStop(bool stop) { Watcher::stop = stop; }
	static void *watch(void *arg); // watch over a directory for any new files
	static void walkAndRegisterDirectories(int fd, string dir);
	static void registerDirectory(int fd, string dir);
};

#endif