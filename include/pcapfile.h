#ifndef PCAPFILE
#define PCAPFILE

#include "typedef.h"
#include <fstream>
#include <sys/stat.h>
#include <unordered_map>
#include "ipv4address.h"
#include "ipv6address.h"
#include <string.h>
using namespace std;

/*
*Class PcapFile updates and keep track of different packet type's count
* and also stores count of each unique ip address.
*/
class PcapFile
{
private:
    const char *fileName;
    const char *filePath;
    const char *fileFolderPath;
    u_int4 IPV4Count;
    u_int4 IPV6Count;
    u_int4 TCPCount;
    u_int4 UDPCount;
    unordered_map<ipv4address, u_int4> uniqueIPV4Count; //to store each ipv4 unique address count
    unordered_map<ipv6address, u_int4> uniqueIPV6Count; //to store each ipv6 unique address count

public:
    PcapFile()
    {
        fileName = new char[1000];
        filePath = new char[1000];
        IPV4Count = 0;
        IPV6Count = 0;
        TCPCount = 0;
        UDPCount = 0;
    }
    PcapFile(const char *name, const char *path)
    {
        fileName = new char[1000];
        filePath = new char[1000];
        fileName = name;
        filePath = path;
    }

    ~PcapFile() { 
        delete []fileName;
        delete []filePath;
        delete []fileFolderPath;
        }
    //Returns file name
    const char *getFileName() { return fileName; }

    //Set file name
    void setFileName(const char *name) { fileName = name; }

    //Returns path of a file
    const char *getFilePath() { return filePath; }

    //Set path of a file
    void setFilePath(const char *path) { filePath = path; }

    //Returns path of a file
    const char *getFileFolderPath() { return fileFolderPath; }

    //Set path of a file
    void setFileFolderPath(const char *path) { fileFolderPath = path; }

    //increment IPV4 count by 1
    void incrementIPV4Count() { IPV4Count++; }

    //return IPV4 count
    u_int4 getIPV4Count() { return IPV4Count; }

    //increment IPV6 count by 1
    void incrementIPV6Count() { IPV6Count++; }

    //return IPV4 count
    u_int4 getIPV6Count() { return IPV6Count; }

    //increment IPV4 address count
    void updateIPV4UniqueCount(ipv4address source, ipv4address destination) { ++uniqueIPV4Count[source]; ++uniqueIPV4Count[destination]; }

    //increment IPV6 address count
    void updateIPV6UniqueCount(ipv6address source, ipv6address destination) { ++uniqueIPV6Count[source]; ++uniqueIPV6Count[destination]; }

    //Increment TCP packet count
    void incrementTCPCount() { TCPCount++; }

    //Returns TCP packet count
    u_int4 getTCPCount() { return TCPCount; }

    //Increment UDP packet count
    void incrementUDPCount() { UDPCount++; }

    //Returns UDP packet count
    u_int4 getUDPCount() { return UDPCount; }

    //will return file name without extension
    string getFileNameWithoutExtension();

    //Prints count of each IPV4 address
    void printIPV4UniqueCount();

    //Prints count of each IPV6 address
    void printIPV6UniqueCount();

    //write IPV4 and IPV6 address's count to file
    void writeIPUniquecount();

    //Returns file size in bytes
    long getFileSize();

    //Returns true if file is accessible else false
    bool isAccessible();

    //Returns last modified time
    string getFileMtime();
};

#endif