#ifndef IPV4HEADER
#define IPV4HEADER
#include "typedef.h"
#include "ipv4address.h"
#include <iostream>
using namespace std;

/*
*This structure is used to store all the fields present in an IPV4  packet. 
*/
typedef struct ipv4header
{
	u_char1 version_headerlen;
	u_char1 service_field;
	u_int2 total_length;
	u_int2 identification;
	u_int2 flags_fragementoffset;
	u_char1 ttl;
	u_char1 protocol;
	u_int2 checksum;
	ipv4address source_ip;		//4 bytes
	ipv4address destination_ip; //4 bytes

	ipv4header()
	{
		protocol = 0;
		total_length = 0;
	}

} pcap_ipv4_hdr;

#endif
