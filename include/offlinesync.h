#ifndef OFFLINESYNC
#define OFFLINESYNC
#include "../include/pcapfile.h"
#include "../include/pcapparser.h"
#include <unordered_map>
#include <sstream>
#include <sys/types.h>
#include <cstring>
#include <dirent.h>
#include<iostream> 
#include<string> 

using namespace std;
class OfflineSync
{
private:
   unordered_map<string, string> index;
   static OfflineSync *offlinesync;
   OfflineSync(){}
    ~OfflineSync(){}
public:
    static OfflineSync* getInstance();
    void updateIndex(PcapFile *pcapfile);
    void writeIndex();
    void readIndex();
    void parseFiles(string folderpath);  
};

#endif