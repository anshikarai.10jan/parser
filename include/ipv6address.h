#ifndef IPV6ADDRESS
#define IPV6ADDRESS ;
#include <iostream>
using namespace std;

/*
*Structure to define ipv4 address type.
*/
typedef struct ipv6address
{
    unsigned char ipv6_address[17]; //16 bytes

    //overloadng == operator to compare 2 ipv6 address
    bool operator==(const ipv6address &address) const
    {
        return (ipv6_address[0] == address.ipv6_address[0] && ipv6_address[1] == address.ipv6_address[1] && ipv6_address[2] == address.ipv6_address[2] && ipv6_address[3] == address.ipv6_address[3] && ipv6_address[4] == address.ipv6_address[4] && ipv6_address[5] == address.ipv6_address[5] && ipv6_address[6] == address.ipv6_address[6] && ipv6_address[7] == address.ipv6_address[7] && ipv6_address[8] == address.ipv6_address[8] && ipv6_address[9] == address.ipv6_address[9] && ipv6_address[10] == address.ipv6_address[10] && ipv6_address[11] == address.ipv6_address[11] && ipv6_address[12] == address.ipv6_address[12] && ipv6_address[13] == address.ipv6_address[13] && ipv6_address[14] == address.ipv6_address[14] && ipv6_address[15] == address.ipv6_address[15] && ipv6_address[16] == address.ipv6_address[16]);
    }

    // Returns string containing ipv4 address in standard format.
    string getIPAddress()
    {
        char ip[100] = "";
        sprintf(ip, "%x%02x:%x%02x:%x%02x:%x%02x:%x%02x:%x%02x:%x%02x:%x%02x", 
                ipv6_address[0], ipv6_address[1], ipv6_address[2], ipv6_address[3],
                ipv6_address[4], ipv6_address[5], ipv6_address[6], ipv6_address[7],
                ipv6_address[8], ipv6_address[9], ipv6_address[10], ipv6_address[11],
                ipv6_address[12], ipv6_address[13], ipv6_address[14], ipv6_address[15]);
        return ip;
    }

} ipv6address;

namespace std
{
//creating a hash function for ipv6
template <>
struct hash<ipv6address>
{
    size_t operator()(const ipv6address &k) const
    {
        size_t res = 17;
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[0]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[1]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[2]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[3]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[4]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[5]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[6]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[7]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[8]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[9]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[10]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[11]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[12]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[13]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[14]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[15]);
        res = res * 31 + hash<unsigned char>()(k.ipv6_address[16]);

        return res;
    }
};
} // namespace std

#endif