#ifndef IPV6HEADER
#define IPV6HEADER
#include "typedef.h"
#include "ipv6address.h"
#include <iostream>
using namespace std;

/*
*This structure is used to store all the fields present in an IPV6  packet. 
*/
typedef struct ipv6header
{
	u_int4 version_priority_flowlable;
	u_int2 payload_length;
	u_char1 protocol;
	u_char1 hop_Limit;
	ipv6address source_ip;		//16 byte
	ipv6address destination_ip; //16 byte

	ipv6header()
	{
		version_priority_flowlable = 0;
		protocol = 0;
		payload_length = 0;
		hop_Limit = 0;
	}

} pcap_ipv6_hdr;

#endif
