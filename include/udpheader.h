#ifndef UDPHEADER
#define UDPHEADER
#include "typedef.h"
#include <iostream>
using namespace std;

/*
*Structure to define udp header.
*/
typedef struct udpheader
{
	u_int2 source_port;
	u_int2 destination_port;
	u_int2 length;
	u_int2 checksum;

	udpheader()
	{
		source_port = 0;
		destination_port = 0;
		length = 0;
		checksum = 0;
	}

} pcap_udp_hdr;

#endif