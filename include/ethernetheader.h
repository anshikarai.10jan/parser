#ifndef ETHERNETHEADER
#define ETHERNETHEADER
#include "typedef.h"
#include "macaddress.h"
#include <iostream>
using namespace std;

/* 
* ethernet_hdr_struct stores all the fields present in ethernet header.
*/
typedef struct ethernet_hdr_struct
{
	macaddress destination; 	//6 bytes
	macaddress source;		//6 bytes
	u_int2 packettype;		//stores type of packet it encapsulates

	ethernet_hdr_struct()
	{
		packettype = 0;
	}

} pcap_ethernet_hdr;

#endif
