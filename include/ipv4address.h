#ifndef IPV4ADDRESS
#define IPV4ADDRESS ;
#include <iostream>
#include "typedef.h"
using namespace std;

/*
*Structure to define ipv4 address type.
*/
typedef struct ipv4address
{
    u_int4 ip_address;

    ipv4address()
    {
        ip_address = 0;
    }

    //overloadng == operator to compare 2 ipv6 address
    bool operator==(const ipv4address &address) const
    {
        return (ip_address == address.ip_address);
    }

    //will return string containing ipv4 address in standard format.
    string getIPAddress()
    {
        unsigned char bytes[4];
        char ip[100] = "";
        bytes[0] = ip_address & 0xFF;
        bytes[1] = (ip_address >> 8) & 0xFF;
        bytes[2] = (ip_address >> 16) & 0xFF;
        bytes[3] = (ip_address >> 24) & 0xFF;
        sprintf(ip, "%d.%d.%d.%d", bytes[0], bytes[1], bytes[2], bytes[3]);
        return ip;
    }
} ipv4address;

namespace std
{
//creating a hash function for ipv6
template <>
struct hash<ipv4address>
{
    size_t operator()(const ipv4address &k) const
    {
        size_t res = 17;
        res = res * 31 + hash<unsigned int>()(k.ip_address);
        return res;
    }
};
} 

#endif