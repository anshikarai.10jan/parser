#ifndef MACADDRESS
#define MACADDRESS
#include <iostream>
using namespace std;

/*
*Structure to define mac address type.
*/
typedef struct macaddress
{
    unsigned char mac_address[6];

    // Prints mac address according to standard format.
    string getMacAddress()
    {
        char mac[17] = "";
        sprintf(mac, "%x:%x:%x:%x:%x:%x", mac_address[0], mac_address[1], mac_address[2], mac_address[3], mac_address[4], mac_address[5]);
        return mac;
    }
} macaddress;

#endif