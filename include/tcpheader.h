#ifndef TCPHEADER
#define TCPHEADER
#include "typedef.h"
#include <iostream>
using namespace std;

/*
*Structure to define tcp header.
*/
typedef struct tcpheader
{
	u_int2 source_port;
	u_int2 destination_port;
	u_int4 sequence_number;
	u_int4 ack_number;
	u_int2 headerlength_flags;
	u_int2 window_size;
	u_int2 checksum;
	u_int2 urgent_pointer;

	tcpheader()
	{
		source_port = 0;
		destination_port = 0;
		sequence_number = 0;
		ack_number = 0;
		headerlength_flags = 0;
		window_size = 0;
		checksum = 0;
		urgent_pointer = 0;
	}

	
} pcap_tcp_hdr;

#endif